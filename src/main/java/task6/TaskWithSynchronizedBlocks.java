package task6;

public class TaskWithSynchronizedBlocks {

    long A = 0;

    Object lock = new Object();

    void method1() {
        synchronized (lock) {
            System.out.println("Method 1 called");
            A++;
        }
    }

    synchronized void method2() {
        synchronized (lock) {
            System.out.println("Method 2 called");
            A++;
        }
    }

    synchronized void method3() {
        synchronized (lock) {
            System.out.println("Method 3 called");
            A++;
        }
    }

    public static void main(String[] args) throws InterruptedException {
        TaskWithSynchronizedMethods task = new TaskWithSynchronizedMethods();
        Thread t1 = new Thread(() -> {
            for (int i = 0; i < 50000; i++)
                task.method1();
        });
        Thread t2 = new Thread(() -> {
            for (int i = 0; i < 50000; i++)
                task.method2();
        });
        Thread t3 = new Thread(() -> {
            for (int i = 0; i < 50000; i++)
                task.method3();
        });
        t1.start();
        t2.start();
        t3.start();

        t1.join();
        t2.join();
        t3.join();

        System.out.println("A = " + task.A);
    }

}
