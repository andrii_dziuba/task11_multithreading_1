package task4;

import task2.Fibonacci;

import java.util.concurrent.*;

public class FibonacciCallable {

    private Callable<Integer> fibonacci(int N) {
        int currentNumber = 1;
        int previousNumber = 0;
        int sum = 0;
        while (currentNumber <= N) {
            sum+=currentNumber;
            int k = previousNumber;
            previousNumber = currentNumber;
            currentNumber += k;
        }
        int finalSum = sum;
        return () -> finalSum;
    }

    public void submit(int n) {
        ExecutorService executor = Executors.newSingleThreadExecutor();

        Future<Integer> integerFuture = executor.submit(new FibonacciCallable().fibonacci(n));

        try {
            System.out.println("Sum of Fibonacci sequence to " + n + ": " + integerFuture.get());
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        executor.shutdown();
    }

    public static void main(String[] args) {
        FibonacciCallable fibonacciCallable = new FibonacciCallable();
        fibonacciCallable.submit(500);
        fibonacciCallable.submit(2000);
        fibonacciCallable.submit(50);
        fibonacciCallable.submit(1000000);
    }
}
