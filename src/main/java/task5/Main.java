package task5;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

public class Main {

    public static void main(String[] args) {
        if (args[0] == null) return;
        int quantity = Integer.parseInt(args[0]);
        for (int i = 0; i < quantity; i++) {
            ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);

            executor.submit(new ScheduledTask());

            executor.shutdown();
        }
    }
}
