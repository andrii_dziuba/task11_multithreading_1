package task5;

import java.time.LocalTime;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

public class ScheduledTask implements Runnable {


    @Override
    public void run() {
        long begin = System.currentTimeMillis();
        try {
            TimeUnit.SECONDS.sleep(ThreadLocalRandom.current().nextInt(10) + 1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        long end = System.currentTimeMillis();
        System.out.println("I slept for " + (end-begin)/1000 + " seconds");
    }
}
