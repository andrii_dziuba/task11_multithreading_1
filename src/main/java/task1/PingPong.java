package task1;

public class PingPong {

    private static Object monitor = new Object();

    class Message implements Runnable {

        private String message;

        public Message(String mes) {
            this.message = mes;
        }

        public void run() {
            while (true) {
                try {
                    monitor.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                System.out.println(message);

                monitor.notify();
                try {
                    Thread.currentThread().sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void startThreads() {
        Thread t1 = new Thread(new Message("Ping"));
        Thread t2 = new Thread(new Message("Pong"));
        t1.start();
        t2.start();
    }

    public static void main(String[] args) {
        new Thread(() -> {
            synchronized (monitor) {
                while (true) {
                    try {
                        monitor.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    System.out.println("Ping");
                    monitor.notify();
                }
            }
        }).start();

        new Thread(() -> {
            synchronized (monitor) {
                while (true) {
                    monitor.notify();

                    System.out.println("Pong");

                    try {
                        monitor.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }
}
