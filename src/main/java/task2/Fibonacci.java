package task2;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadLocalRandom;

public class Fibonacci {

    /**
     * This method prints an array of <code>long</code> and fills in numbers from Fibonacci sequence
     * @param N
     */
    private void fibonacci(int N) {
        int currentNumber = 1;
        int previousNumber = 0;
        while (currentNumber <= N) {
            System.out.print(currentNumber + " ");
            int k = previousNumber;
            previousNumber = currentNumber;
            currentNumber += k;
        }
    }

    public static void main(String[] args) {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        time();
        executor.execute(() -> new Fibonacci().fibonacci(1000));

        executor.shutdown();
        while(!executor.isTerminated());
        timePrintResult();
        executor = Executors.newFixedThreadPool(4);
        time();
        executor.execute(() -> new Fibonacci().fibonacci(1000));
        executor.shutdown();
        while(!executor.isTerminated());
        timePrintResult();
    }

    static long begin;
    public static void time() {
        begin = System.nanoTime();
    }

    public static void timePrintResult() {
        System.out.println("Time passed: " + (System.nanoTime() - begin) + " ns");
    }

}
