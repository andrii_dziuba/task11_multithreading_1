package task7;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

public class PipedStream {

    public static void main(String[] args) {
        PipedInputStream pipedInputStream = new PipedInputStream();
        PipedOutputStream pipedOutputStream = new PipedOutputStream();

        try {
            pipedInputStream.connect(pipedOutputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }

        new Thread(() -> {
            for (char c : "Dziuba Andrii".toCharArray()) {
                try {
                    pipedOutputStream.write(c);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();

        new Thread(() -> {
            int read;
            try {
                while ((read = pipedInputStream.read()) != -1) {
                    System.out.println((char) read);
                    Thread.currentThread().sleep(100);
                }
            } catch (IOException | InterruptedException e) {
            }
        }).start();
    }
}
