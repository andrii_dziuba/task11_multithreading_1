***Task 1*** Wait-notify sequence  
  
ping  
pong  
...  As expected  
  
***Task 2, 3*** Fibonacci sequence with executor  
  
1 1 2 3 5 8 13 21 34 55 89 144 233 377 610 987 Time passed: 75337356 ns // newSingleThreadExecutor  
1 1 2 3 5 8 13 21 34 55 89 144 233 377 610 987 Time passed: 734634 ns // newFixedThreadPool(4)  
  
***Task 4*** Fibonacci callable usage  
  
Sum of Fibonacci sequence to 500: 986  
Sum of Fibonacci sequence to 2000: 4180  
Sum of Fibonacci sequence to 50: 88  
Sum of Fibonacci sequence to 1000000: 2178308  
  
***Task 5*** Scheduled task  
  
I slept for 2 seconds  
I slept for 3 seconds  
I slept for 5 seconds  
I slept for 6 seconds  
  
***Task 6*** Task with syncronized blocks and methods works as required  
  
***Task 7*** Piped stream works as expected